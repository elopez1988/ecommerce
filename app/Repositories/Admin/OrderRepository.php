<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Order;
use App\Repositories\BaseRepository;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

/**
 * Class OrderRepository
 * @package App\Repositories\Admin
 * @version July 30, 2020, 7:17 pm UTC
*/

class OrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'total',
        'impuesto',
        'status',
        'commet',
        'user_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Order::class;
    }

    public function store($input, $product)
    {
        try {
            DB::beginTransaction();

            $model = new Order;
            $model->user_id = $input['user_id'];
            $model->status  = $input['status'];
            $model->commet  = $input['commet'];
            $total = 0;
            // foreach ($product as $key => $product) {
            //     $total   += $product->price;
            // }

            $model->total = $product[0]->price + ($product[0]->price * $product[0]->tax)/100;
            $model->tax   = $product[0]->tax;

            $model->save();
            $model->orderProducts()->attach($product[0]->id);
    
            DB::commit();
            return $model;
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            // if ($th instanceof QueryException) {
            //     $this->handleMySQLException($th);
            // }
        }
    }

    public function prices($model)
    {
        # code...
    }
}
