<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateOrderRequest;
use App\Http\Requests\Admin\UpdateOrderRequest;
use App\Repositories\Admin\OrderRepository;
use App\Http\Controllers\AppBaseController;
use App\Mail\sendOrder;
use App\Repositories\Admin\ProductRepository;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Mail;
use Response;

class OrderController extends AppBaseController
{
    /** @var  OrderRepository */
    private $orderRepository;

    /** @var  ProductRepository */
    private $productRepository;

    public function __construct(OrderRepository $orderRepo,ProductRepository $productRepo)
    {
        $this->orderRepository = $orderRepo;
        $this->productRepository = $productRepo;
    }

    /**
     * Display a listing of the Order.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $orders = $this->orderRepository->all();

        return view('admin.orders.index')
            ->with('orders', $orders);
    }

    /**
     * Show the form for creating a new Order.
     *
     * @return Response
     */
    public function create()
    {
        $users    = \App\User::all()->pluck('name', 'id');
        $products = $this->productRepository->all()->pluck('name', 'id');
        
        return view('admin.orders.create')
            ->with('products', $products)
            ->with('users', $users);
    }

    /**
     * Store a newly created Order in storage.
     *
     * @param CreateOrderRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderRequest $request)
    {
        $input     = $request->only('user_id','status','commet');
        $productId = $request->only('product_id');

        $product = $this->productRepository->find($productId);
    
        $order = $this->orderRepository->store($input,$product);

        $order->load('user','orderProducts');

        $destinatario = $order['user']['email'];

        $response = Mail::send('emails.sendOrder', $order->toArray(), function ($message) use ($destinatario) {
            $message->to($destinatario)->subject('Order');});

        Flash::success('Order saved successfully.');

        return redirect(route('admin.orders.index'));
    }

    /**
     * Display the specified Order.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('admin.orders.index'));
        }

        $order->load('user','orderProducts');

        return view('admin.orders.show')->with('order', $order);
    }

    /**
     * Show the form for editing the specified Order.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('admin.orders.index'));
        }

        $users    = \App\User::all()->pluck('name', 'id');
        $products = $this->productRepository->all()->pluck('name', 'id');

        return view('admin.orders.edit')
            ->with('order', $order)
            ->with('products', $products)
            ->with('users', $users);
    }

    /**
     * Update the specified Order in storage.
     *
     * @param int $id
     * @param UpdateOrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderRequest $request)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('admin.orders.index'));
        }

        $order = $this->orderRepository->update($request->all(), $id);

        Flash::success('Order updated successfully.');

        return redirect(route('admin.orders.index'));
    }

    /**
     * Remove the specified Order from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('admin.orders.index'));
        }

        $this->orderRepository->delete($id);

        Flash::success('Order deleted successfully.');

        return redirect(route('admin.orders.index'));
    }

    /**
     * Display the specified Order.
     *
     * @param int $id
     *
     * @return Response
     */
    public function createPDF($id)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('admin.orders.index'));
        }

        $order->load('user','orderProducts');

        $pdf = \PDF::loadView('pdf.order',['order'=>$order]);
        return $pdf->download('order-'.$order->id.'.pdf');
    }
}
