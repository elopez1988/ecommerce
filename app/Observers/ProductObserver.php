<?php

namespace App\Observers;

use App\Models\Admin\Log;
use App\Models\Admin\Product;

class ProductObserver
{
    /**
     * Handle the product "created" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function created(Product $product)
    {
        $data = [
            'type_model' => 'Product',
            'type_method' => 'creation',
            'id_model' => $product->id,
        ];

        $model = new log;
        $model->create($data);
    }

    /**
     * Handle the product "updated" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function updated(Product $product)
    {
        $data = [
            'type_model' => 'Product',
            'type_method' => 'update',
            'id_model' => $product->id,
        ];

        $model = new log;
        $model->create($data);
    }

    /**
     * Handle the product "deleted" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function deleted(Product $product)
    {
        $data = [
            'type_model' => 'Product',
            'type_method' => 'deleted',
            'id_model' => $product->id,
        ];

        $model = new log;
        $model->create($data);
    }

    /**
     * Handle the product "restored" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function restored(Product $product)
    {
        //
    }

    /**
     * Handle the product "force deleted" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function forceDeleted(Product $product)
    {
        $data = [
            'type_model' => 'Product',
            'type_method' => 'forceDeleted',
            'id_model' => $product->id,
        ];

        $model = new log;
        $model->create($data);
    }
}
