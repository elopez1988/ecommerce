<?php

namespace App\Observers;

use App\Models\Admin\Order;
use App\Models\Admin\Log;

class OrderObserver
{
    /**
     * Handle the order "created" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function created(Order $order)
    {
        $data = [
            'type_model' => 'Order',
            'type_method' => 'created',
            'id_model' => $order->id,
        ];

        $model = new log;
        $model->create($data);
    }

    /**
     * Handle the order "updated" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function updated(Order $order)
    {
        $data = [
            'type_model' => 'Order',
            'type_method' => 'updated',
            'id_model' => $order->id,
        ];

        $model = new log;
        $model->create($data);
    }

    /**
     * Handle the order "deleted" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function deleted(Order $order)
    {
        $data = [
            'type_model' => 'Order',
            'type_method' => 'deleted',
            'id_model' => $order->id,
        ];

        $model = new log;
        $model->create($data);
    }

    /**
     * Handle the order "restored" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function restored(Order $order)
    {
        //
    }

    /**
     * Handle the order "force deleted" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function forceDeleted(Order $order)
    {
        $data = [
            'type_model' => 'Order',
            'type_method' => 'forceDeleted',
            'id_model' => $order->id,
        ];

        $model = new log;
        $model->create($data);
    }
}
