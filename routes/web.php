<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->middleware('verified');

Route::group(['prefix' => 'admin'], function () {
    Route::resource('products', 'Admin\ProductController', ["as" => 'admin']);
    Route::resource('orders', 'Admin\OrderController', ["as" => 'admin']);
    Route::get('orders/pdf/{id}', 'Admin\OrderController@createPDF', ["as" => 'admin'])->name('admin.orders.createPDF');
});

