<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {

    return [
        'total' => $faker->randomDigitNotNull,
        'impuesto' => $faker->randomDigitNotNull,
        'status' => $faker->word,
        'commet' => $faker->word,
        'user_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
