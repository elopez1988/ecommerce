<!DOCTYPE>
<html>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <meta http-equiv="X-UA-Compatible" content="ie=edge"> -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Reporte de venta</title>
    <style>
        body {
        /*position: relative;*/
        /*width: 16cm;  */
        /*height: 29.7cm; */
        /*margin: 0 auto; */
        /*color: #555555;*/
        /*background: #FFFFFF; */
        font-family: Arial, sans-serif; 
        font-size: 14px;
        /*font-family: SourceSansPro;*/
        }
 
        #logo{
        float: left;
        margin-top: 1%;
        margin-left: 2%;
        margin-right: 2%;
        }
 
        #imagen{
            margin-top: 10px;
            width: 20%;
            height: 45px;
        }
 
        #datos{
        float: left;
        margin-top: 0%;
        margin-left: 2%;
        margin-right: 2%;
        /*text-align: justify;*/
        }
 
        #encabezado{
        text-align: center;
        margin-left: -15%;
        margin-right: 30%;
        font-size: 15px;
        }
 
        #fact{
        /*position: relative;*/
        float: right;
        margin-top: 2%;
        margin-left: 2%;
        margin-right: 2%;
        font-size: 20px;
        }
 
        section{
        clear: left;
        }
 
        #cliente{
        text-align: left;
        }
 
        #facliente{
        width: 40%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
        }

        #fatotal{
        width: 40%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
        }
 
        #fac, #fv, #fa{
        color: #FFFFFF;
        font-size: 15px;
        }
 
        #facliente thead{
        padding: 20px;
        background: #1b6fba;
        text-align: left;
        border-bottom: 1px solid #FFFFFF;  
        }
 
        #facvendedor{
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
        }
 
        #facvendedor thead{
        padding: 20px;
        background: #1b6fba;
        text-align: center;
        border-bottom: 1px solid #FFFFFF;  
        }
 
        #facarticulo{
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
        }
 
        #facarticulo thead{
        padding: 20px;
        background: #1b6fba;
        text-align: center;
        border-bottom: 1px solid #FFFFFF;  
        }
        #gracias{
        text-align: center; 
        }
    </style>
    <body>
        <header>
            <div id="logo">
                <img src="logo/shopping-cart.png" alt="shopping-cart" id="imagen">
            </div>
            <div id="datos">
                <p id="encabezado">
                    <b>eCommerce Test</b><br>Caracas, Venezuela
                </p>
            </div>
            <div id="fact">
                <p>N° de Factura<br>
                <span style="text-align: right;">{{ $order->id }}</span></p>
            </div>
        </header>
        <br>
        <!-- Client -->
        <section>
            <div>
                <table id="facliente">
                    <thead>                        
                        <tr>
                            <th id="fac">Cliente</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th><p id="cliente">Nombre ó Razon Social: {{ $order->user->name }}<br>
                            Email: {{ $order->user->email }}</p></th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
        <br>
        <!-- Products -->
        <section>
            <div>
                <table id="facarticulo">
                    <thead>
                        <tr id="fa">
                            <th>ITEMS</th>
                            <th>CANT</th>
                            <th>NOMBRE</th>
                            <th>PRECIO UNIT</th>
                            <th>IMPUESTO</th>
                            <th>PRECIO TOTAL</th>
                        </tr>
                    </thead>
                    <tbody border="1">
                    @foreach ($order->orderProducts as $key => $product)
                        <tr>
                            <td style="text-align: center;">{{ $key + 1 }}</td>
                            <td style="text-align: center;">1</td>
                            <td>{{ $product['name'] }}</td>
                            <td style="text-align: center;">{{ number_format($product['price'], 2, ',', '.' ) }}</td>
                            <td style="text-align: center;">{{ number_format($product['tax'], 2, ',', '.' ) }}</td>
                            <td style="text-align: center;">{{ number_format($product['price'] + ($product['price'] * $product['tax'])/100, 2, ',', '.' ) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <table id="fatotal">
                <tbody>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>SUBTOTAL</th>
                            <td>$ {{ number_format($order->total - $order->tax, 2, ',', '.' ) }}</td>
                        </tr>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Impuesto</th>
                            <td>$ {{ number_format($order->tax, 2, ',', '.' ) }}</td>
                        </tr>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>TOTAL</th>
                            <td>$ {{ number_format($order->total, 2, ',', '.' ) }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
        <br>
        <br>
        <footer>
            <div id="gracias">
                <p><b>Gracias por su compra!</b></p>
            </div>
        </footer>
    </body>
</html>