@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Order
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'admin.orders.store']) !!}

                        @include('admin.orders.fields',[
                            'products' => $products,
                            'users' => $users
                        ])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
