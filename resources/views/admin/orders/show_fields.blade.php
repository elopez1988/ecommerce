<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User:') !!}
    <p>{{ $order->user->name }}</p>
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $order->status }}</p>
</div>

<div class="form-group col-sm-12">
    <div class="table-responsive">   
        <table class="table">
            <thead>
                <tr>
                    <th>ITEMS</th>
                    <th>CANT</th>
                    <th>NOMBRE</th>
                    <th>PRECIO UNIT</th>
                    <th>IMPUESTO %</th>
                    <th>PRECIO TOTAL</th>
                </tr>
            </thead>
            <tbody border="1">
            @foreach ($order->orderProducts as $key => $product)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>1</td>
                    <td>{{ $product['name'] }}</td>
                    <td>{{ number_format($product['price'], 2, ',', '.' ) }}</td>
                    <td>{{ number_format($product['tax'], 2, ',', '.' ) }}</td>
                    <td>{{ number_format($product['price'] + ($product['price'] * $product['tax'])/100, 2, ',', '.' ) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div> 
</div>

<!-- Impuesto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('impuesto', 'Impuesto:') !!}<p>{{ number_format($order->tax, 2, ',', '.' ) }}</p>
    
</div>

<!-- Total Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total', 'Total:') !!}     <p>{{ number_format($order->total, 2, ',', '.' ) }}</p>

</div>

<!-- Commet Field -->
<div class="form-group col-sm-12">
    {!! Form::label('commet', 'Commet:') !!}
    <p>{{ $order->commet }}</p>
</div>