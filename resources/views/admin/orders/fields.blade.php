<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'Usuario') !!}
    {!! Form::select('user_id',@$users, null, ['class' => 'form-control','placeholder' => 'Seleccionar Usuario']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Estatus:') !!}
    {!! Form::select('status', ['Pedido' => 'Pedido', 'Procesando' => 'Procesando','Completado' => 'Completado'], null, ['class' => 'form-control', 'placeholder' => 'Seleccionar estatus...']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('product_id', 'Productos') !!}
    {!! Form::select('product_id', @$products, null, ['class' => 'form-control','placeholder' => 'Seleccionar Productos','required' => 'required']) !!}
</div>

<!-- Commet Field -->
<div class="form-group col-sm-12">
    {!! Form::label('commet', 'Commet:') !!}
    {!! Form::textarea('commet', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.orders.index') }}" class="btn btn-default">Cancel</a>
</div>