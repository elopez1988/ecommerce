<div class="table-responsive">
    <table class="table" id="orders-table">
        <thead>
            <tr>
                <th>#</th>
                <th>User</th>
                <th>Total</th>
                <th>Status</th>

                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($orders as $order)
            <tr>

                <td>{{ $order->id }}</td>
                <td>{{ $order->user->name }}</td>
                <td>{{ number_format($order->total, 2, ',', '.' ) }}</td>
                <td>{{ $order->status }}</td>
                <td>
                    {!! Form::open(['route' => ['admin.orders.destroy', $order->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('admin.orders.show', [$order->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('admin.orders.createPDF', [$order->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-print"></i></a>
                        <a href="{{ route('admin.orders.edit', [$order->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>