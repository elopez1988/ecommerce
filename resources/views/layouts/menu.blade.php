<li class="{{ Request::is('admin/products*') ? 'active' : '' }}">
    <a href="{{ route('admin.products.index') }}"><i class="glyphicon glyphicon-th"></i><span>Products</span></a>
</li>

<li class="{{ Request::is('admin/orders*') ? 'active' : '' }}">
    <a href="{{ route('admin.orders.index') }}"><i class="glyphicon glyphicon-shopping-cart"></i><span>Orders</span></a>
</li>

